#! /bin/sh

#check for root privileges
check_priv (){
	if [ $(id -u) -ne 0 ] 
	then
       		echo "please run this script with root privilege's"
		return 1
	else
		main	
		return 0
	fi
}

main (){	
	mv ./50-inforpi /etc/update-motd.d/50-inforpi
	chmod u+x /etc/update-motd.d/50-inforpi
	mv ./issue.net /etc/issue.net
	chmod u+x /etc/issue.net
	echo -n "Banner /etc/issue.net" >> /etc/ssh/sshd_config
	service sshd restart
}

check_priv
